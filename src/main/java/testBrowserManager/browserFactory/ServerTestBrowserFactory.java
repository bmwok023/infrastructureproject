package testBrowserManager.browserFactory;

import testBrowserManager.TestConfigManager;
import testBrowserManager.api.BrowserType;

public class ServerTestBrowserFactory implements TestBrowserCreator {

    @Override
    public String createTestBrowser() {
        String BrowserName = TestConfigManager.getInstance().getTestBrowser();
        BrowserType browserType = BrowserType.valueOf(BrowserName);

        switch (browserType) {
            case CHROME:
                System.out.println("Chrome");
            case IE:
                System.out.println(("IE"));
            case FIREFOX:
                System.out.println("Firefox");
            case SAFARI:
                System.out.println("Safari");

        }
        return null;
    }
}

