package testBrowserManager.browserFactory;

public interface TestBrowserCreator {

    String createTestBrowser();
}
