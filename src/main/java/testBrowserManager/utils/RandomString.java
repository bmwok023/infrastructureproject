package testBrowserManager.utils;

public class RandomString {

    public static void main(String[] args) {
    }

    public enum Mode {
        ALPHA,
        NUMERIC,
        APLPHANUMERIC;
    }

    public static String randomString(Mode mode, int length) {
        String randomString = "";
        String characters;

        switch (mode) {

            case ALPHA:
                characters = "abcdifgigklmopqrstuwywz";
                break;
            case NUMERIC:
                characters = "12344556789";
                break;
            case APLPHANUMERIC:
            default:
                characters = "abcdifgigklmopqrstuwywz12344556789";
                break;
        }
        int characterslength = characters.length();

        for (int i = 0; i < length; i++) {
            double index = Math.random() * characterslength;
            randomString += characters.charAt((int) index);
        }
        return randomString;
    }


}


