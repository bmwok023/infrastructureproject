package testBrowserManager.utils;

import java.util.HashMap;

public class UrlBuilder {

    private String protocol;
    private String domain;
    private String port;
    private String path;
    private HashMap<String, String> param;


    public UrlBuilder() {

    }

    public UrlBuilder withProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public UrlBuilder withDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public UrlBuilder withPort(String port) {
        this.port = port;
        return this;
    }

    public UrlBuilder withPath(String path) {
        this.path = path;
        return this;
    }

    public UrlBuilder withParameters(String param) {
        this.param.put(param, null);
        return this;
    }

    public UrlBuilder withParameters(String key, String value) {
        this.param.put(key, value);
        return this;
    }

    public UrlBuilder withParameter(HashMap parameters) {
        this.param = param;
        return this;

    }

    public String build() {
        String str = "";
        for (String key : param.keySet()) {
            str += param.get(key) + "&";
        }

        return protocol + "://" + domain + "/" + path + "?" + str;

    }
}
