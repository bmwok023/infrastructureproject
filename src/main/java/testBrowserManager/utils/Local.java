package testBrowserManager.utils;

public enum Local {

    ENGLISH ("eu","en_US","English"),
    RUSSIAN("ru","ru_RU","Русский"),
    GERMAN("de", "de_DE", "Deutch");

    private String code;
    private String encoding;
    private String name;

    Local(String code, String encoding, String name) {
        this.code = code;
        this.encoding = encoding;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
