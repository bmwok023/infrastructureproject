package testBrowserManager.testLogger;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StdLogger extends TestLogger {

    @Override
    public void log(String logs) {
        String output = step + ") " + timeNow() + " [" + currentProcess() + "]: " + logs;
        System.out.println(output);
        step++;

    }

    private int step = 1;

    private String timeNow() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        return sdf.format(date);
    }

    private String currentProcess() {
        return Thread.currentThread().getName();
    }

}
