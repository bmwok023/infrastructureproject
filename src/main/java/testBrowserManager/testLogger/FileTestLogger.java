package testBrowserManager.testLogger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;

public class FileTestLogger extends TestLogger {
    private String fileName = "report" + date() + "txt";
    FileWriter fw = null;

    @Override
    public void log(String logs) {
        try (FileWriter fw = new FileWriter(new File(fileName), true)) {
            fw.write("" + "\n");
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
/*
        finally {
            try {
                fw.close();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
*/
    }
}
