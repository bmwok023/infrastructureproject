package testBrowserManager.data.user;


import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        try {
            UserDataMapper udf = new DefaultUserDataMapper();

            System.out.println(udf.getUserByEmail("user2@test.com"));

            System.out.println(udf.getUserByName("user3"));

        } catch (UserNotFoundException ex) {

            System.out.println(ex);
        }

    }

}
