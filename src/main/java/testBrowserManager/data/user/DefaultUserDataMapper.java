package testBrowserManager.data.user;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DefaultUserDataMapper implements UserDataMapper {

    private List<User> users = new ArrayList<>();

    public DefaultUserDataMapper() {
        try {

            BufferedReader br = new BufferedReader(new FileReader("src/users.txt"));
            String line = null;

            while ((line = br.readLine()) != null) {

                String[] data = line.split(",");
                users.add(new User(data[0], data[1], data[2]));
            }

        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Users file not found");
        } catch (IOException ex) {

            throw new RuntimeException("Users file can't be read");
        }
    }

    @Override
    public User getUserByEmail(String email) {
        User user = null;
        for (User u:users){
        if (u.getUserEmail().equals(email))
            user =u;
        }
        if (user == null)

            throw new UserNotFoundException("User with email " + email + " not found");
        return user;
    }

    @Override
    public User getUserByName(String name) {
        User user = null;
        for (User u:users){
            if (u.getUserName().equals(name))
                user =u;

        }
        if (user == null)

            throw new UserNotFoundException("User with name " + name + " not found");
        return user;
    }
}
