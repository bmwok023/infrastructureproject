package testBrowserManager.data.user;

public interface UserDataMapper {


    User getUserByEmail(String email);

    User getUserByName(String name);

}
