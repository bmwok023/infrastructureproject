package testBrowserManager.api;

public interface TestBrowserManager {


    String getTestBrowser();

    void destroyTestBrowser(String testBrowser);
}
