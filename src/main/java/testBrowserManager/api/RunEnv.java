package testBrowserManager.api;


public enum RunEnv {

    LOCAL,
    BUILDSERVER,
    REMOTE;

}
