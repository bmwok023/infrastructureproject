package testBrowserManager.api;


public enum BrowserType {

    CHROME,
    IE,
    FIREFOX,
    SAFARI
}
