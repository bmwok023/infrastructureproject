package testBrowserManager.api;

import testBrowserManager.TestConfigManager;
import testBrowserManager.browserFactory.LocalTestBrowserFactory;
import testBrowserManager.browserFactory.TestBrowserCreator;

public class DefaultTestBrowserManager implements TestBrowserManager {

    @Override
    public String getTestBrowser() {

        String env = TestConfigManager.getInstance().getTestEnv();
        RunEnv runEnv = RunEnv.valueOf(env);
        switch (runEnv) {
            case LOCAL:
                TestBrowserCreator localTestBrowserFactory = new LocalTestBrowserFactory();
                System.out.println("local");
            case BUILDSERVER:
                TestBrowserCreator serverTestBrowserFactory = new LocalTestBrowserFactory();
                System.out.println("buildServer");
            case REMOTE:
                TestBrowserCreator cloudTestBrowserFactory = new LocalTestBrowserFactory();
                System.out.println("Cloud");

        }

        return null;
    }

    @Override
    public void destroyTestBrowser(String testBrowser) {

        System.out.println("Session was destroyed");

    }

}



