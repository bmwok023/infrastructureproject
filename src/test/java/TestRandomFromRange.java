import org.junit.Assert;
import org.junit.Test;
import testBrowserManager.utils.RandomFromRange;


public class TestRandomFromRange {

    @Test
    public void testRandomNumber() {
        int min = 0;
        int max = 5;
        Assert.assertTrue(min <= RandomFromRange.randomFromRange(max, min));
        Assert.assertTrue(max >= RandomFromRange.randomFromRange(max, min));
    }


}
