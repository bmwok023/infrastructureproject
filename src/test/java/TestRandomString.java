import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

import testBrowserManager.utils.RandomString;

import static org.hamcrest.MatcherAssert.assertThat;


public class TestRandomString {

    @Test
    public void testRandomString() {
        String alphaString = RandomString.randomString(RandomString.Mode.ALPHA, 9);
        String numericString = RandomString.randomString(RandomString.Mode.NUMERIC, 9);
        String alphaNumericString = RandomString.randomString(RandomString.Mode.APLPHANUMERIC, 9);

        assertThat(alphaString, notNullValue());
        assertThat(numericString, notNullValue());
        assertThat(alphaNumericString, notNullValue());

        Assert.assertTrue(alphaString.length() <= 9);


    }


}
